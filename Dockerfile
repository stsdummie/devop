FROM ptell/ubu_py_cv_flask:v1.1
 
# Puerto HTTP por defecto para uWSGI
ARG UWSGI_HTTP_PORT=5000
ENV UWSGI_HTTP_PORT=$UWSGI_HTTP_PORT
 
# Aplicacion por defecto para uWSGI
ARG UWSGI_APP=webapp
ENV UWSGI_APP=$UWSGI_APP
 
# Se copia el contenido de la aplicacion
COPY ["WebApp", "/WebApp"]
#COPY WebApp /WebApp
 
# Se copia el fichero con la configuración de uWSGI
COPY uwsgi.ini uwsgi.ini
 
# Se establece el directorio de trabajo
WORKDIR /WebApp
 
# Se crea un volume con el contenido de la aplicacion
VOLUME /WebApp
 
# Se inicia uWSGI
ENTRYPOINT ["uwsgi", "--ini", "/uwsgi.ini"]