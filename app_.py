#import os
from flask import Flask
from flask_pymongo import PyMongo
from bson.json_util import dumps

#Se crea la instancia Flask
app = Flask(__name__)

#Se configura el acceso a la base de datos mongodb
app.config['MONGO_DBNAME'] = 'empleados'
app.config['MONGO_URI'] = 'mongodb://mongo:27017/empleados'
#app.config["MONGO_URI"] = 'mongodb://' + os.environ['MONGODB_USERNAME'] + ':' + os.environ['MONGODB_PASSWORD'] + '@' + os.environ['MONGODB_HOSTNAME'] + ':27017/' + os.environ['MONGODB_DATABASE']

#Se asocia la configuración pasando la app
mongo = PyMongo(app)


#Se define la ruta raiz con metodo POST
@app.route('/',methods=['POST'])
def index():
    #Se realiza la conexion a la coleccion empleados
    empleados = mongo.db.empleados

    #Se hace la consulta y se devuelve  en formato json_util
    resultados = empleados.find()
    return dumps(resultados)


if __name__ == "__main__":
    #Se corre la aplicacion en modo debug
    app.run(host="0.0.0.0",debug=True)
