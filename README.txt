Proyecto montado en docker
refrescar: sudo bash start.sh

docker run -d -it --name base ubu_py_cv_flask:v1.1
1. descargamos los archivos de git

2.crear el contenedor db de mongoDB montando un volumen:
sudo docker volume create dbdata
sudo docker run -d --name db --mount src=dbdata,dst=/data/db mongo

3.crear base "uno" con la coleccion "empleados" y anadir documentos
(entrar al contenedor de mongo)
sudo docker exec -it db bash 
(correr mongo)
mongo
(ver ls bases existentes)
show dbs
(crear base "uno")
use uno
(insertar documentos a la coleccion "empleados")
db.empleados.insert([{"nombre": "Jane Doe", "sexo": "femenino", "edad": 29.0}, {"nombre": "John Doe", "sexo": "masculino", "edad": 39.0}, {"nombre": "Pedro Perez", "sexo": "masculino", "edad": 55.0}, {"nombre": "Petra", "sexo": "femenino", "edad": 65.0}])
(para ver todo lo agregado a la coleccion)
db.empleados.find({})
.................................
{ "_id" : ObjectId("6018f2cb3429a21592222471"), "nombre" : "Jane Doe", "sexo" : "femenino", "edad" : 29 }
{ "_id" : ObjectId("6018f2cb3429a21592222472"), "nombre" : "John Doe", "sexo" : "masculino", "edad" : 39 }
{ "_id" : ObjectId("6018f2cb3429a21592222473"), "nombre" : "Pedro Perez", "sexo" : "masculino", "edad" : 55 }
{ "_id" : ObjectId("6018f2cb3429a21592222474"), "nombre" : "Petra", "sexo" : "femenino", "edad" : 65 }
.................................
exit
exit

4.en la carpeta raiz (FLASK-DOCKER) debemos ver el dockerfile, creamos la imagen base (utiliza: ptell/ubu_py_cv_flask:v1.1):

sudo docker build -t sts_app .

5.crear el conteiner de la app exponiendo puertos
	(sudo docker run --name app -d --rm -p 8081:5000 sts_app)
	sudo docker run --name app -d -it --rm -p 8081:5000 sts_app
	sudo docker run --name app -d -it -p 8081:5000 sts_app
	sudo docker run --name app -it -p 8081:5000 sts_app

(sin mongo)

sudo docker run --name app -it --rm -p 5000:5000 sts_app
echo $FLASK_APP
sudo docker run --name app -it --rm -p 5000:5000 -e FLASK_APP='/home/flask/main.py' sts_app
(con mongo)
sudo docker run -d --name app -p 8081:5000 --env MONGO_URL=mongodb://db:27017 sts_app


sudo docker build -t sts_app3 .

sudo docker run --name app -d --restart unless-stopped -p 8080:5000 -v $(pwd)/WebApp:/WebApp sts_app3
sudo docker run --name app -d --rm -p 8080:5000 -v $(pwd)/WebApp:/WebApp sts_app3



6.crear red virtual de docker y agregar los dos contenedores, db y app
sudo docker network create --attachable badaknet
sudo docker network connect badaknet db
sudo docker network connect badaknet app
