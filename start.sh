#!/bin/bash
sudo docker stop app
sudo docker rmi sts_app
sudo docker build -t sts_app .
sudo docker run --name app -d --rm -p 5001:5000 -v $(pwd)/WebApp:/WebApp sts_app
#sudo docker run --name app -it --rm -p 5000:5000 sts_app